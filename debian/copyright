Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Guitarix
Source: https://guitarix.org or (https://sourceforge.net/projects/guitarix/)
Upstream-Contact:
    Guitarix devs <guitarix-developer@lists.sourceforge.net>
    Hermann Meyer (brummer)
    Andreas Degert (adegert)
    Pete Shorthose (kickback)
    Markus Schmidt (boomshop)
Comment: DFSG repackaged to unpack waf. See lintian source-contains-waf-binary

Files: *
Copyright:
    2008-2022 Guitarix devs
License: GPL-2+

Files: org.guitarix.guitarix.metainfo.xml
Copyright: 2022 Guitarix devs
License: FSFAP

Files: fonts/*
Copyright:
    2011 Google Inc. All Rights Reserved.
License: Apache-2.0

Files: glade-gxw/glade-gxw.c
Copyright:
    2009-2010 Hermann Meyer
    2009-2010 James Warden
    2009-2010 Andreas Degert
License: GPL-2+

Files: IR/BestPlugins_Amps/* IR/BestPlugins_Bands/*
Copyright:
    2013 David Fau Casquel
License: GPL-2+

Files: libgxw/* libgxwmm/*
Copyright:
    2009-2010 Hermann Meyer
    2009-2010 James Warden
    2009-2010 Andreas Degert
License: GPL-2+

Files: pixmaps/* rcstyles/*
Copyright:
    2009-2010 Hermann Meyer
    2009-2010 James Warden
    2009-2010 Andreas Degert
License: GPL-2+

Files:
    src/LV2/*
    src/faust/*
    src/faust-generated/*
Copyright:
    2012 Hermann Meyer
    2012 Andreas Degert
    2012 Pete Shorthose
    2012 Steve Poskitt
License: GPL-2+

Files:
    src/LV2/gx_gcb_95.lv2/gx_gcb_95.cpp
    src/LV2/gx_gcb_95.lv2/gx_gcb_95.ttl
    src/LV2/gx_gcb_95.lv2/gx_gcb_95.h
    src/LV2/gx_hogsfoot.lv2/gx_hogsfoot.ttl
    src/LV2/gx_hogsfoot.lv2/gx_hogsfoot.h
    src/LV2/gx_hogsfoot.lv2/gx_hogsfoot.cpp
    src/LV2/gx_hornet.lv2/gx_hornet.h
    src/LV2/gx_hornet.lv2/gx_hornet.cpp
    src/LV2/gx_hornet.lv2/gx_hornet.ttl
    src/LV2/gx_mxrdist.lv2/gx_mxrdist.cpp
    src/LV2/gx_mxrdist.lv2/gx_mxrdist.ttl
    src/LV2/gx_mxrdist.lv2/gx_mxrdist.h
    src/LV2/gx_scream.lv2/gx_scream.cpp
    src/LV2/gx_scream.lv2/gx_scream.h
    src/LV2/gx_scream.lv2/gx_scream.ttl
    src/LV2/gx_mbreverb.lv2/gx_mbreverb.h
    src/LV2/gx_mbreverb.lv2/gx_mbreverb.cpp
    src/LV2/gx_mbreverb.lv2/gx_mbreverb.ttl
    src/LV2/gx_bmp.lv2/gx_bmp.ttl
    src/LV2/gx_bmp.lv2/gx_bmp.cpp
    src/LV2/gx_bmp.lv2/gx_bmp.h
    src/LV2/gx_digital_delay.lv2/gx_digital_delay.ttl
    src/LV2/gx_digital_delay.lv2/gx_digital_delay.h
    src/LV2/gx_fuzzfacefm.lv2/gx_fuzzfacefm.h
    src/LV2/gx_digital_delay.lv2/gx_digital_delay.cpp
    src/LV2/gx_fuzzfacefm.lv2/gx_fuzzfacefm.ttl
    src/LV2/gx_fuzzfacefm.lv2/gx_fuzzfacefm.cpp
    src/LV2/gx_fumaster.lv2/gx_fumaster.ttl
    src/LV2/gx_fumaster.lv2/gx_fumaster.h
    src/LV2/gx_fumaster.lv2/gx_fumaster.cpp
    src/LV2/gx_room_simulator.lv2/gx_room_simulator.cpp
    src/LV2/gx_room_simulator.lv2/gx_room_simulator.ttl
    src/LV2/gx_room_simulator.lv2/gx_room_simulator.h
    src/LV2/gx_bossds1.lv2/gx_bossds1.h
    src/LV2/gx_bossds1.lv2/gx_bossds1.cpp
    src/LV2/gx_bossds1.lv2/gx_bossds1.ttl
    src/LV2/gx_cstb.lv2/gx_cstb.h
    src/LV2/gx_cstb.lv2/gx_cstb.ttl
    src/LV2/gx_cstb.lv2/gx_cstb.cpp
    src/LV2/gx_oc_2.lv2/gx_oc_2.cpp
    src/LV2/gx_oc_2.lv2/gx_oc_2.ttl
    src/LV2/gx_oc_2.lv2/gx_oc_2.h
    src/LV2/gx_vibe.lv2/gx_vibe.cpp
    src/LV2/gx_vibe.lv2/gx_vibe.h
    src/LV2/gx_vibe.lv2/gx_vibe.ttl
    src/LV2/gx_vibe.lv2/gx_vibe_mono.ttl
    src/LV2/gx_fuzzface.lv2/gx_fuzzface.h
    src/LV2/gx_fuzzface.lv2/gx_fuzzface.ttl
    src/LV2/gx_fuzzface.lv2/gx_fuzzface.cpp
    src/LV2/gx_shimmizita.lv2/gx_shimmizita.ttl
    src/LV2/gx_detune.lv2/gx_detune.ttl
    src/LV2/gx_detune.lv2/gx_detune.cpp
    src/LV2/gx_detune.lv2/gx_detune.h
    src/LV2/gx_mole.lv2/gx_mole.ttl
    src/LV2/gx_mole.lv2/gx_mole.cpp
    src/LV2/gx_mole.lv2/gx_mole.h
    src/LV2/gx_susta.lv2/gx_susta.h
    src/LV2/gx_susta.lv2/gx_susta.cpp
    src/LV2/gx_susta.lv2/gx_susta.ttl
    src/LV2/gx_rangem.lv2/gx_rangem.h
    src/LV2/gx_rangem.lv2/gx_rangem.ttl
    src/LV2/gx_rangem.lv2/gx_rangem.cpp
    src/LV2/gx_muff.lv2/gx_muff.ttl
    src/LV2/gx_muff.lv2/gx_muff.h
    src/LV2/gx_muff.lv2/gx_muff.cpp
    src/LV2/gx_aclipper.lv2/gx_aclipper.ttl
    src/LV2/gx_aclipper.lv2/gx_aclipper.h
    src/LV2/gx_aclipper.lv2/gx_aclipper.cpp
    src/LV2/gx_colwah.lv2/gx_colwah.h
    src/LV2/gx_colwah.lv2/gx_colwah.cpp
    src/LV2/gx_colwah.lv2/wah.h
    src/LV2/gx_colwah.lv2/gx_colwah.ttl
    src/LV2/gx_hfb.lv2/gx_hfb.h
    src/LV2/gx_hfb.lv2/gx_hfb.cpp
    src/LV2/gx_hfb.lv2/gx_hfb.ttl
    src/LV2/gx_digital_delay_st.lv2/gx_digital_delay_st.cpp
    src/LV2/gx_digital_delay_st.lv2/gx_digital_delay_st.ttl
    src/LV2/gx_digital_delay_st.lv2/gx_digital_delay_st.h
    src/LV2/gx_switched_tremolo.lv2/gx_switched_tremolo.ttl
    src/LV2/gx_livelooper.lv2/gx_livelooper.cpp
    src/LV2/gx_livelooper.lv2/gx_livelooper.h
    src/LV2/gx_livelooper.lv2/gx_livelooper.ttl
Copyright: 2014 Guitarix project MOD project
License: GPL-2+

Files:
    src/LV2/gx_detune.lv2/detune.cc
Copyright:
    1999-2009 Stephan M. Bernsee <smb [AT] dspdimension [DOT] com>
    2014 Hermann Meyer
License: Wide-Open-License
 The Wide Open License (WOL)
 .
 Permission to use, copy, modify, distribute and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice and this license appear in all source copies.
 THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF
 ANY KIND. See https://www.dspguru.com/wol.htm for more information.

Files: src/ladspa/ladspa_guitarix.cpp
Copyright: 2011-2012 Andreas Degert, Hermann Meyer
           2011-2012 Hermann Meyer
License: GPL-2+

Files: ladspa/*
Copyright:
    2008-2010 Hermann Meyer (brummer)
License: GPL-2+

Files: ladspa/distortion.cpp ladspa/guitarix-ladspa.cpp
Copyright:
    2008 Hermann Meyer (brummer)
    2008 Julius O. Smith <jos@ccrma.stanford.edu>
License: BSD-3-Clause

Files: po/de.pot
Copyright:
    2010 Hermann Meyer
License: GPL-2+

Files: po/es.po
Copyright:
    2011-2012 Pablo Fernández <pablo.fbus@gmail.com>
License: GPL-2+

Files: po/fr.po
Copyright:
    2010-2011 Jean-Yves Poilleux <jy.poilleux@gmail.com>
    2012 Frédéric Rech <f.rech@yahoo.fr>
    2013 bayo <bayo.fr@gmail.com>
    2020 Olivier Humbert <trebmuh@tuxfamily.org>
License: GPL-2+

Files: po/it.po
Copyright:
    2011 Ivan Tarozzi <itarozzi@gmail.com>
License: GPL-2+

Files: tools/*
Copyright:
    2009-2010 Hermann Meyer
    2009-2010 James Warden
    2009-2010 Andreas Degert
License: GPL-2+

Files: tools/ampsim/tensbs/*.f
Copyright: cmlib authors
License: public-domain
 Authors explicitly licensed it as such. The Fortran routines are from
 cmlib.

Files: tools/faustmod.pyx tools/gx_tube_transfer.py
Copyright:
    2009-2010 Hermann Meyer
    2009-2010 James Warden
    2009-2010 Andreas Degert
    2011 Pete Shorthose
License: GPL-2+

Files: tools/plugins/oc_2/op777.mod
Copyright:
    2016 Fedor Uporov <thisisadrgreenthumb@gmail.com>
License: GPL-2+

Files: src/plugins/abgate.cc
Copyright: 2011 Antanas Bružas
License: LGPL-3+

Files:
    src/zita-convolver/*
    src/zita-convolver-ffmpeg/*
    src/zita-resampler-*/*
Copyright: 2006-2012 Fons Adriaensen <fons@linuxaudio.org>
License: GPL-3+

Files: debian/*
Copyright: 2008 brummer <brummer-@web.de>
           2010-2015 Roland Stigge <stigge@antcom.de>
           2015-2017 Víctor Cuadrado Juan <me@viccuad.me>
           2019-2020 Olivier Humbert <trebmuh@tuxfamily.org>
           2020-2022 Dennis Braun <d_braun@kabelmail.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 The name of the author may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache 2.0 License
 can be found in /usr/share/common-licenses/Apache-2.0 file.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.
